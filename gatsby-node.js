/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.com/docs/node-apis/
 */

// You can delete this file if you're not using it
exports.createPages = async function ({ actions, graphql }) {
  let result = await graphql(`{
  allMicrocmsEssay(sort: {publishedAt: DESC}) {
    edges {
      node {
        essayId
        publishedAt(formatString: "YYYY")
      }
    }
  }
  allMicrocmsNews(sort: {publishedAt: DESC}) {
    edges {
      node {
        newsId
        publishedAt(formatString: "YYYY")
      }
    }
  }
  allMicrocmsWorks(sort: {publishedAt: DESC}) {
    edges {
      node {
        worksId
        publishedAt(formatString: "YYYYMM")
        tags {
          id
        }
      }
    }
  }
  allMicrocmsArtworks(sort: {publishedAt: DESC}) {
    edges {
      node {
        artworksId
        publishedAt(formatString: "YYYYMM")
      }
    }
  }
  allMicrocmsBooks(sort: {publishedAt: DESC}) {
    edges {
      node {
        booksId
        publishedAt(formatString: "YYYYMM")
      }
    }
  }
  allMicrocmsWorksTag {
    edges {
      node {
        title
        worksTagId
      }
    }
  }
}`)
  const essayYears = new Set();
  const essayYearsArray = [];
  result.data.allMicrocmsEssay.edges.forEach(edge => {
    let id = edge.node.essayId;
    actions.createPage({
      path: `/essay/${id}`,
      component: require.resolve(`./src/templates/essay/essay.js`),
      context: {
        id: id ,
      },
    })
    if(essayYearsArray.indexOf(edge.node.publishedAt) === -1){
      essayYearsArray.push(edge.node.publishedAt)
    }
    let essayyear = edge.node.publishedAt;
    essayYears.add(essayyear);
    essayYears.forEach(year => {
      actions.createPage({
        path: `/essay/${year}/`,
        component: require.resolve(`./src/templates/essay/archive.js`),
        context: {
          displayYear: year,
          periodStartDate: `${year}-01-01T00:00:00.000Z`,
          periodEndDate: `${year}-12-31T23:59:59.999Z`,
          years:essayYearsArray
        }
      });
    });
  })


  let newsYears = [];
  result.data.allMicrocmsNews.edges.forEach(edge => {
    let id = edge.node.newsId;
    actions.createPage({
      path: `/news/${id}`,
      component: require.resolve(`./src/templates/news/news.js`),
      context: {
        id: id ,
      },
    })

    let newsYear = edge.node.publishedAt;
    if(newsYears.indexOf(newsYear) === -1){
      newsYears.push(newsYear);
    }
    newsYears.forEach(year => {
      actions.createPage({
        path: `/news/${year}/`,
        component: require.resolve(`./src/templates/news/archive.js`),
        context: {
          displayYear: year,
          periodStartDate: `${year}-01-01T00:00:00.000Z`,
          periodEndDate: `${year}-12-31T23:59:59.999Z`,
          years:newsYears
        }
      });
    });
  })

  result.data.allMicrocmsWorks.edges.forEach(edge => {
    let id = edge.node.worksId;
    let tags = [];
    edge.node.tags.forEach(tag => {
      tags.push(tag.id);
    })
    actions.createPage({
      path: `/works/${id}`,
      component: require.resolve(`./src/templates/works/works.js`),
      context: {
        id: id ,
        tags:tags
      },
    })
  })
  result.data.allMicrocmsArtworks.edges.forEach(edge => {
    let id = edge.node.artworksId;
    actions.createPage({
      path: `/personal/${id}`,
      component: require.resolve(`./src/templates/personal/personal.js`),
      context: {
        id: id ,
      },
    })
  })
  result.data.allMicrocmsBooks.edges.forEach(edge => {
    let id = edge.node.booksId;
    actions.createPage({
      path: `/books/${id}`,
      component: require.resolve(`./src/templates/books/books.js`),
      context: {
        id: id ,
      },
    })
  })
  result.data.allMicrocmsWorksTag.edges.forEach(edge => {
    let id = edge.node.worksTagId;
    let title = edge.node.title;
    actions.createPage({
      path: `/works/tags/${id}`,
      component: require.resolve(`./src/templates/works/tags/works.js`),
      context: {
        id: id ,
        title:title
      },
    })
  })
}

exports.createSchemaCustomization = ({ actions }) => {
  const { createTypes } = actions

 //type名はパスカルケースで宣言すること
  const typeDefs = `
  type MicrocmsArtworks implements Node {
    movie:String
  }
  type MicrocmsWorks implements Node {
    video:String
  }
  `
  createTypes(typeDefs)
}