require("dotenv").config()

module.exports = {
  siteMetadata: {
    title: `写真家 砺波周平`,
    description: ``,
    author: `@gatsbyjs`,
    siteUrl: 'https://tonami.vercel.app/'
  },
  plugins: [
    `gatsby-plugin-sitemap`,
    /*
    {
      resolve: `gatsby-plugin-canonical-urls`,
      options: {
        siteUrl: `https://crossing-lines.vercel.app`,
        stripQueryString: true,
      },
    },
    {
      resolve: 'gatsby-plugin-robots-txt',
      options: {
        host: 'https://crossing-lines.vercel.app',
        sitemap: 'https://crossing-lines.vercel.app/sitemap.xml',
        policy: [{ userAgent: '*', disallow: ['/']}]
      }
    },
    */
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-image`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby-starter-default`,
        short_name: `starter`,
        start_url: `/`,
        display: `minimal-ui`,
        icon: `src/images/gatsby-icon.png`, // This path is relative to the root of the site.
      },
    },
    `gatsby-plugin-gatsby-cloud`,
    {
      resolve: 'gatsby-source-microcms',
      options: {
        apiKey: '4d1f0dbf3cc14f33824ba431c0ca4335aa53',
        serviceId: 'tonami',
        apis: [{
          endpoint: 'essay',
        },
        {
          endpoint: 'works',
        },
        {
          endpoint: 'works_tag',
        },
        {
          endpoint: 'artworks'
        },
        {
          endpoint: 'books'
        },
        {
          endpoint: 'news'
        }
      ],
      },
    },
    {
      resolve: `gatsby-plugin-sass`,
      options: {
      },
    },
    `gatsby-plugin-svgr-svgo`,
    {
      resolve: `gatsby-plugin-scroll-reveal-with-new-react`,
      options: {
          threshold: 0.2, // Percentage of an element's area that needs to be visible to launch animation
          rootMargin: '0% 50%', // Corresponds to root's bounding box margin
      }
    },
    {
      resolve: `gatsby-plugin-google-gtag`,
      options: {
        trackingIds: ["G-26X260FZPE"],
      },
    },
  ]
}
