import React, { useState, createContext, useContext } from "react"

const SiteContext = createContext({
  page: '',
  setPage: () => {},
})

const SiteProvider = ({ children }) => {
  const [page, setPage] = useState('')

  return <SiteContext.Provider value={{ page, setPage }}>
    {children}
  </SiteContext.Provider>
}

const useSiteContext = () => useContext(SiteContext)

export { SiteProvider, useSiteContext}