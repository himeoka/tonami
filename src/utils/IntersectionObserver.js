import Player from '@vimeo/player';
export const handleObserver = (targets) => {
  targets.current.forEach(target => {
    const observer = new IntersectionObserver(entries => {
      entries.forEach(entry => {
        if (entry.isIntersecting) {
          var iframe = entry.target.querySelector('iframe[title="vimeo-player"]');
          if(iframe){
            var player = new Player(iframe);
            player.setLoop(true)
            player.setAutopause(false)
            player.setMuted(true)
            player.getPaused().then(function(paused) {
              if(paused){
                  player.play();
              }
            });
            observer.unobserve(entry.target);
          }
        }
      })
    })
    observer.observe(target)
  })

}