//import React, { useState, useEffect, useRef } from 'react';
import React from 'react';
import { graphql } from 'gatsby'

import Layout from "../../components/layout"
import Seo from "../../components/seo"
import Essay from "../../components/essay"
const EssayPage = ({ data }) => {
  return (
    <Layout>
      <Seo title={`${data.microcmsEssay.title} | エッセイ一覧`} shareImage={data.microcmsEssay.mainImage.url}/>
      <div className="p_essay">
        <div className="p_essay_inner inner_tb_pc">
          <Essay data={data.microcmsEssay} />
        </div>
      </div>
    </Layout>
  )
}

export default EssayPage
export const query = graphql`
 query($id: String!) {
   microcmsEssay(essayId: { eq: $id }) {
    id
    essayId
    publishedAt
    mainImage {
      height
      url
      width
    }
    photo {
      image {
        height
        url
        width
      }
      fieldId
    }
    text
    title
    essayId
  }
 }
`
