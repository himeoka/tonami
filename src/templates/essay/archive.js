//import React, { useState, useEffect, useRef } from 'react';
import React from 'react';
import { graphql } from 'gatsby'
import { Link } from "gatsby"

import Layout from "../../components/layout"
import Seo from "../../components/seo"
import Essay from "../../components/essay"
const yearPage = ({ data,pageContext}) => {
  const yearIndex = pageContext.years.indexOf(pageContext.displayYear);
  let prev,next;
  if(yearIndex > 0){
    next = pageContext.years[yearIndex-1]
  }
  if(yearIndex < pageContext.years.length){
    prev = pageContext.years[yearIndex+1]
  }
  console.log(prev,next)
  return (
    <Layout>
      <Seo title={`${pageContext.displayYear} | エッセイ一覧`} />
      <div className="p_essay">
        <div className="p_essay_inner inner_tb_pc">
         <p className="p_essay_ttl">{pageContext.displayYear}</p>
         {data.allMicrocmsEssay.edges.map (({ node }) => (
            <Essay data={node} key={node.id}/>
         ))}
         <div className="p_essay_nav">
          {next &&
            <Link to={`/essay/${next}/`}>{next}</Link>
          }
          {prev &&
            <Link to={`/essay/${prev}/`}>{prev}</Link>
          }
         </div>
        </div>
      </div>
    </Layout>
  )
}

export const query = graphql`query ($periodStartDate: Date, $periodEndDate: Date) {
  allMicrocmsEssay(
    sort: {publishedAt: DESC}
    filter: {publishedAt: {gte: $periodStartDate, lt: $periodEndDate}}
  ) {
    edges {
      node {
        id
        publishedAt
        mainImage {
          height
          url
          width
        }
        photo {
          image {
            height
            url
            width
          }
          fieldId
        }
        text
        title
        essayId
      }
    }
  }
}`

export default yearPage
