import React, { useEffect } from 'react';
import { Link, graphql } from 'gatsby'
import Layout from "../../components/layout"
import Seo from "../../components/seo"
import Imgix from "react-imgix"
import { useSiteContext} from "../../context/site-mode"
import dayjs from 'dayjs';



const BooksContent = ({books,allbooks,tags}) => {
  const { page, setPage } = useSiteContext()
  useEffect(() => {
    setPage("books")
    return () => {
      setPage("")
    }
  });
  return (
    <div className="p_books_detail">
    <div className="p_books_detail_inner inner_tb_pc">
      <div className="inner_sp">
        <h1 className="p_books_detail_ttl">{books.title}</h1>
        <p className="p_books_detail_date">{dayjs(books.publishedAt).format(`YYYY.MM.DD`)}</p>
      </div>
      <div className="p_books_detail_mainimage" data-sal="fade" data-sal-duration="1000">
          <figure className={"p_books_detail_image "  + (books.mainImage.height > books.mainImage.width ? "tate" : "")}>
            <span className="p_books_detail_image_inner" style={{paddingTop: (books.mainImage.height/books.mainImage.width)*100 + "%"}}>
              <Imgix
                className="lazyload"
                src={books.mainImage.url}
                sizes="(max-width: 700px) 100vw, 700px"
                htmlAttributes={{
                alt:books.title,
                src:"data:image/gif;base64,R0lGODlhAQABAGAAACH5BAEKAP8ALAAAAAABAAEAAAgEAP8FBAA7"
                }}
                attributeConfig={{
                  src: "data-src",
                  srcSet: "data-srcset",
                  sizes: "data-sizes",
                }}
                imgixParams={{q:85}}
              />
            </span>
          </figure>
      </div>
      <div className="inner_sp" data-sal="fade" data-sal-duration="1000" >
        <p className="p_books_detail_text" dangerouslySetInnerHTML={{__html: books.lead}}></p>
        <div className="p_books_detail_text lh" dangerouslySetInnerHTML={{__html: books.text}}></div>
      </div>
      <div className="p_books_detail_gallery">
        {books.photo.map(( node,index ) => (
          <figure data-sal="fade" data-sal-duration="1000" key={index} className={"p_books_detail_gallery_image p_books_detail_image " + (node.image.height > node.image.width ? "tate" : "")}>
            <span className="p_books_detail_image_inner" style={{paddingTop: (books.mainImage.height/books.mainImage.width)*100 + "%"}}>
              <Imgix
                className="lazyload"
                src={node.image.url}
                sizes="(max-width: 700px) 100vw, 700px"
                htmlAttributes={{
                alt: books.title,
                src:"data:image/gif;base64,R0lGODlhAQABAGAAACH5BAEKAP8ALAAAAAABAAEAAAgEAP8FBAA7"
                }}
                attributeConfig={{
                  src: "data-src",
                  srcSet: "data-srcset",
                  sizes: "data-sizes",
                }}
                imgixParams={{q:85}}
              />
            </span>
          </figure>
        ))}
      </div>
    </div>
  </div>
  )
}


const BooksPage = ({ data }) => {
  const books = data.microcmsBooks;
  return (
    <Layout>
      <Seo title={`${books.title} | BOOKS`} shareImage={books.mainImage.url} />
      <BooksContent books={books}/>
    </Layout>
  )
}

export default BooksPage
export const query = graphql`
 query($id: String!) {
   microcmsBooks(booksId: { eq: $id }) {
    id
    booksId
    mainImage {
      height
      url
      width
    }
    photo {
      image {
        height
        url
        width
      }
      fieldId
    }
    text
    title
    lead
    publishedAt
  }
}
`
