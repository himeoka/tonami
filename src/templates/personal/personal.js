import React, { useEffect } from 'react';
import { graphql } from 'gatsby'
import Layout from "../../components/layout"
import Seo from "../../components/seo"
import Swiper from "../../components/swiper"
import { useSiteContext} from "../../context/site-mode"

const WorksContent = ({artworks}) => {
  const { page, setPage } = useSiteContext()
  useEffect(() => {
    setPage("personal")
    const handleResize = () => {
      let height = window.innerHeight;
      document.documentElement.style.setProperty( '--vh', height/100 + 'px');
    }
    handleResize();
    window.addEventListener('resize', handleResize)
    return () => {
      setPage("")
      window.removeEventListener('resize', handleResize)
    }
  });
  return (
    <>
      <div className="p_artworks_detail" data-sal="fade" data-sal-duration="1000">
        <div className="p_artworks_detail_inner inner_pc">
          <Swiper data={artworks.photo} title={artworks.title} movie={artworks.movie}/>
          <div className="p_artworks_detail_text inner_sp_tb">
            <h1 className="p_artworks_detail_text_ttl">{artworks.title}</h1>
          </div>
        </div>
      </div>
    </>
  )
}


const WorksPage = ({ data }) => {
  const artworks = data.microcmsArtworks;
  return (
    <Layout>
      <Seo title={`${artworks.title} | PERSONAL一覧`} shareImage={artworks.photo[0].image.url}/>
      <WorksContent artworks={artworks}/>
    </Layout>
  )
}

export default WorksPage
export const query = graphql`
 query($id: String!) {
   microcmsArtworks(artworksId: { eq: $id }) {
    artworksId
    photo {
      image {
        height
        url
        width
      }
      fieldId
    }
    movie
    title
  }
}
`
