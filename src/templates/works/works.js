import React, {useState,useEffect,useRef } from 'react';
import { Link, graphql } from 'gatsby'
import Layout from "../../components/layout"
import Seo from "../../components/seo"
import Imgix from "react-imgix"
import WorksList from "../../components/workslist"
import TagList from "../../components/taglist"
import moment from "moment"
import { useSiteContext} from "../../context/site-mode"
import Player from '@vimeo/player';
import iconGrid from "../../images/cmn/grid.svg";
import iconAll from "../../images/cmn/all.svg";


const WorksContent = ({works,allworks,tags}) => {
  const { page, setPage } = useSiteContext()
  const videoElement = useRef(null)
  const [layout,setLayout] = useState("all")

  const handleClick = (type) => {
    setLayout(type)
    window.localStorage.view = type;
  }
  useEffect(() => {
    if(videoElement.current){
      var iframe = videoElement.current.querySelector('iframe[title="vimeo-player"]');
      if(iframe){
        var player = new Player(iframe);
        player.setAutopause(false)
        player.setMuted(true)
        player.getPaused().then(function(paused) {
          if(paused){
              player.play();
          }
        });
      }
    }
    setPage("works")
    if(window.localStorage.view){
      setLayout(window.localStorage.view)
    }
    return () => {
      setPage("")
    }
  });
  return (
    <div className="p_works_detail">
    <div className="p_works_detail_inner inner_tb_pc">
      <div className="inner_sp">
        <h1 className="p_works_detail_ttl">{works.title}</h1>
        {/*<p className="p_works_detail_date">{moment(works.publishedAt).format(`YYYY.MM.DD`)}</p>*/}
        <div className="p_works_tags_list" aria-expanded={true}>
          <ul>
            {works.tags.map (( node ) => (
                <li key={node.id}>
                    <Link to={`/works/tags/${node.id}`}>{node.title}</Link>
                </li>
            ))}
          </ul>
        </div>
      </div>
      <div className="p_works_detail_mainimage" data-sal="fade" data-sal-duration="1000">
        {works.video &&
          <div ref={videoElement} className="p_works_detail_video" dangerouslySetInnerHTML={{
          __html: works.video
            }}></div>
        }
        {!works.video &&
          <figure className={"p_works_detail_image "  + (works.mainImage.height > works.mainImage.width ? "tate" : "")}>
            <span className="p_works_detail_image_inner" style={{paddingTop: (works.mainImage.height/works.mainImage.width)*100 + "%"}}>
              <Imgix
                className="lazyload"
                src={works.mainImage.url}
                sizes="(max-width: 700px) 100vw, 700px"
                htmlAttributes={{
                alt:works.title,
                src:"data:image/gif;base64,R0lGODlhAQABAGAAACH5BAEKAP8ALAAAAAABAAEAAAgEAP8FBAA7"
                }}
                attributeConfig={{
                  src: "data-src",
                  srcSet: "data-srcset",
                  sizes: "data-sizes",
                }}
                imgixParams={{q:85}}
              />
            </span>
          </figure>
        }
      </div>
      <div className="inner_sp">
        <div className="p_works_detail_text" data-sal="fade" data-sal-duration="1000" dangerouslySetInnerHTML={{__html: works.text}}></div>
      </div>
      <div className={`p_works_detail_gallery ${layout === "grid" ? "grid":""}`}>
        {works.photo.map(( node,index ) => (
          <figure data-sal="fade" data-sal-duration="1000" key={index} className={"p_works_detail_gallery_image p_works_detail_image " + (node.image.height > node.image.width ? "tate" : "")}>
            <span className="p_works_detail_image_inner" style={{paddingTop: (node.image.height/node.image.width)*100 + "%"}}>
              <Imgix
                className="lazyload"
                src={node.image.url}
                sizes="(max-width: 700px) 100vw, 700px"
                htmlAttributes={{
                alt: works.title,
                src:"data:image/gif;base64,R0lGODlhAQABAGAAACH5BAEKAP8ALAAAAAABAAEAAAgEAP8FBAA7"
                }}
                attributeConfig={{
                  src: "data-src",
                  srcSet: "data-srcset",
                  sizes: "data-sizes",
                }}
                imgixParams={{q:85}}
              />
            </span>
          </figure>
        ))}
      </div>
      <div className="p_works_detail_bottom inner_sp">
      <p className="p_works_detail_subttl">似た事例</p>
      <WorksList data={allworks} ctg="works" />
      <div className="p_works_tags">
        <div className="p_works_tags_list" aria-expanded={true}>
            <TagList data={tags} />
        </div>
      </div>
    </div>
    </div>
    <div className="p_works_detail_nav">
      <p className={layout === "all" ? "on":""} onClick={handleClick.bind(this, "all")}><img src={iconAll} alt=""/></p>
      <p className={layout === "grid" ? "on":""} onClick={handleClick.bind(this, "grid")}><img src={iconGrid} alt=""/></p>
    </div>
  </div>
  )
}


const WorksPage = ({ data,pageContext }) => {
  console.log(pageContext)
  const works = data.microcmsWorks;
  const tags = data.allMicrocmsWorksTag.edges;
  const allworks = data.allMicrocmsWorks.edges;
  return (
    <Layout>
      <Seo title={`${works.title} | WORKS`} shareImage={works.mainImage.url} />
      <WorksContent works={works} tags={tags} allworks={allworks}/>
    </Layout>
  )
}

export default WorksPage
export const query = graphql`query ($id: String!, $tags: [String!]!) {
  microcmsWorks(worksId: {eq: $id}) {
    id
    worksId
    mainImage {
      height
      url
      width
    }
    video
    photo {
      image {
        height
        url
        width
      }
      fieldId
    }
    text
    title
    tags {
      id
      title
    }
  }
  allMicrocmsWorks(
    limit: 2
    sort: {publishedAt: DESC}
    filter: {worksId: {ne: $id}, tags: {elemMatch: {id: {in: $tags}}}}
  ) {
    edges {
      node {
        worksId
        publishedAt(formatString: "MMMM DD, YYYY")
        title
        text
        tags {
          id
          title
        }
        mainImage {
          height
          url
          width
        }
      }
    }
  }
  allMicrocmsWorksTag {
    edges {
      node {
        title
        worksTagId
      }
    }
  }
}`

