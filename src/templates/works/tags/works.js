//import React, { useState, useEffect, useRef } from 'react';
import React, { useState } from 'react';
import { Link ,graphql } from "gatsby"

import Layout from "../../../components/layout"
import Seo from "../../../components/seo"
import WorksList from "../../../components/workslist"
import TagList from "../../../components/taglist"

const WorksPage = ({ data, pageContext }) => {
  const [menuActive, setMenuActive] = useState(true);
  return (
    <Layout>
      <Seo title={`${pageContext.title} | Works一覧`} />
      <div className="p_works">
        <div className="p_works_inner inner">
          <h1 className="p_works_ttl"><Link to={`/works/`}>WORKS</Link></h1>
          <div className="p_works_tags">
            <p className="p_works_tags_ttl" aria-expanded={menuActive} onClick={() => setMenuActive(!menuActive)}>Tags</p>
            <div className="p_works_tags_list" aria-expanded={menuActive}>
              <TagList data={data.allMicrocmsWorksTag.edges} active={pageContext.id} />
            </div>
          </div>
          <WorksList data={data.allMicrocmsWorks.edges} ctg="works" />
        </div>
      </div>
    </Layout>
  )
}

export const query = graphql`query ($id: String!) {
  allMicrocmsWorks(
    sort: {publishedAt: DESC}
    filter: {tags: {elemMatch: {id: {eq: $id}}}}
  ) {
    edges {
      node {
        worksId
        publishedAt(formatString: "MMMM DD, YYYY")
        title
        text
        tags {
          id
          title
        }
        mainImage {
          height
          url
          width
        }
        video
      }
    }
  }
  allMicrocmsWorksTag {
    edges {
      node {
        title
        worksTagId
      }
    }
  }
}`

export default WorksPage
