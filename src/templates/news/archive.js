
import React, { useState } from 'react';
import { graphql,Link } from 'gatsby'
import Layout from "../../components/layout"
import Seo from "../../components/seo"
import NewsList from "../../components/newslist"
import ArchiveList from "../../components/archivelist"

const NewsPage = ({data,pageContext}) => {
  console.log(pageContext)
  const news = data.allMicrocmsNews.edges;
  const [menuActive, setMenuActive] = useState(true);
  return (
    <Layout>
      <Seo title={`${pageContext.displayYear} | ニュース一覧`} />
      <div className="p_news">
        <div className="p_news_inner inner">
         <h1 className="p_news_ttl"><Link to={`/news/`}>NEWS</Link></h1>
         <p className="p_news_subttl">{pageContext.displayYear}</p>
         <div className="p_works_tags">
            <p className="p_works_tags_ttl" aria-expanded={menuActive} onClick={() => setMenuActive(!menuActive)}>Archives</p>
            <div className="p_works_tags_list" aria-expanded={menuActive}>
              <ArchiveList data={pageContext.years} active={pageContext.displayYear} />
            </div>
          </div>
         <NewsList data={news} />
        </div>
      </div>
    </Layout>
  )
}

export const query = graphql`query ($periodStartDate: Date, $periodEndDate: Date) {
  allMicrocmsNews(
    sort: {publishedAt: DESC}
    filter: {publishedAt: {gte: $periodStartDate, lt: $periodEndDate}}
  ) {
    edges {
      node {
        publishedAt
        photo {
          image {
            height
            url
            width
          }
        }
        text
        title
        newsId
      }
    }
  }
}`

export default NewsPage
