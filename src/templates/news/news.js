import React, { useEffect } from 'react';
import { graphql,Link } from 'gatsby'
import Layout from "../../components/layout"
import Seo from "../../components/seo"
import Imgix from "react-imgix"
import dayjs from "dayjs"
import { useSiteContext} from "../../context/site-mode"

const NewsContent = ({news}) => {
  const { page, setPage } = useSiteContext()
  const mainImage = news.photo.length ? news.photo[0].image : "";
  useEffect(() => {
    setPage("news")
    return () => {
      setPage("")
    }
  });
  return (
    <div className="p_news_detail">
      <div className="p_news_detail_inner inner">
        <h1 className="p_news_detail_ttl">{news.title}</h1>
        <p className="p_news_detail_date">{dayjs(news.publishedAt).format(`YYYY.MM.DD`)}</p>
        { mainImage && 
        <div className="p_news_detail_mainimage" data-sal="fade" data-sal-duration="1000">
            <figure className={"p_news_detail_image "  + (mainImage.height > mainImage.width ? "tate" : "")}>
              <span className="p_news_detail_image_inner" style={{paddingTop: (mainImage.height/mainImage.width)*100 + "%"}}>
                <Imgix
                  className="lazyload"
                  src={mainImage.url}
                  sizes="(max-width: 700px) 100vw, 700px"
                  htmlAttributes={{
                  alt:news.title,
                  src:"data:image/gif;base64,R0lGODlhAQABAGAAACH5BAEKAP8ALAAAAAABAAEAAAgEAP8FBAA7"
                  }}
                  attributeConfig={{
                    src: "data-src",
                    srcSet: "data-srcset",
                    sizes: "data-sizes",
                  }}
                  imgixParams={{q:85}}
                />
              </span>
            </figure>
        </div>
        }
        <div className="p_news_detail_text"  data-sal="fade" data-sal-duration="1000" dangerouslySetInnerHTML={{__html: news.text}}></div>
        <div className="p_news_detail_gallery">
          {news.photo.map(( node,index ) => {
            if(index > 0){
              return(
                <figure data-sal="fade" data-sal-duration="1000" key={index} className={"p_news_detail_gallery_image p_news_detail_image " + (node.image.height > node.image.width ? "tate" : "")}>
                  <span className="p_news_detail_image_inner" style={{paddingTop: (node.image.height/node.image.width)*100 + "%"}}>
                    <Imgix
                      className="lazyload"
                      src={node.image.url}
                      sizes="(max-width: 700px) 100vw, 700px"
                      htmlAttributes={{
                      alt: news.title,
                      src:"data:image/gif;base64,R0lGODlhAQABAGAAACH5BAEKAP8ALAAAAAABAAEAAAgEAP8FBAA7"
                      }}
                      attributeConfig={{
                        src: "data-src",
                        srcSet: "data-srcset",
                        sizes: "data-sizes",
                      }}
                      imgixParams={{q:85}}
                    />
                  </span>
                </figure>
              )
            } 
          })}
        </div>
        <div className="p_news_detail_bottom">
          <p className="p_news_detail_back"><Link to="/news">戻る</Link></p>
        </div>
      </div>
    </div>
  )
}

const NewsPage = ({ data }) => {
  const news = data.microcmsNews;
  const mainImage = news.photo.length ? news.photo[0].image : "";
  return (
    <Layout>
      <Seo title={`${news.title} | ニュース`} shareImage={mainImage.url}/>
      <NewsContent news={news} />
    </Layout>
  )
}

export default NewsPage
export const query = graphql`
 query($id: String!) {
   microcmsNews(newsId: { eq: $id }) {
    id
    newsId
    publishedAt
    photo {
      image {
        height
        url
        width
      }
      fieldId
    }
    text
    title
    newsId
  }
 }
`
