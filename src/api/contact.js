import fetch from "node-fetch"
export default async function handler(req, res) {
  if (req.method !== "POST"){
    return res.status(400).json({ error: `Method is worng` })
  }else{
    try {
      const SENDGRID_API_KEY = "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"
      const {subject,name,email,tel,company,text} = req.body;
      const send = await fetch('https://api.sendgrid.com/v3/mail/send', {
        body: JSON.stringify({
          "personalizations": [
            {
              "to": [
                {
                  "email": email,
                  "name": name
                }
              ],
              "bcc": [
                {
                  "email": "hello@stoopa.org"
                }
              ],
            }
          ],
          "from": {
            "email": "hello@stoopa.org",
            name: '砺波周平'
          },
          "replyTo": {
            email: 'hello@stoopa.org',
            name: '砺波周平'
          },
          "subject": "【砺波周平】 お問い合わせありがとうございます。",
          "content": [
            {
              "type": "text/plain",
              "value": `お問い合わせくださいありがとうございます。\n下記の内容でお問い合わせを受付いたしました。\n\n件名：${subject}\nお名前：${name}\nメールアドレス：${email}\n電話番号：${tel}\n会社名：${company}\nお問い合わせ内容：${text}\n`
            }
          ]
        }),
        headers: {
          'Authorization': `Bearer ${SENDGRID_API_KEY}`,
          'Content-Type': 'application/json',
        },
        method: 'POST',
      })
      console.log(send)
      return res.status(200).json(send);
    } catch (e) {
      console.log(e)
      return res.status(400).json(e);
    }
  }
}