//import React, { useState, useEffect, useRef } from 'react';
import React,{useState,useEffect} from 'react';
import { graphql } from 'gatsby'
import Layout from "../components/layout"
import Seo from "../components/seo"
import Essay from "../components/essay"
import InfiniteScroll from 'react-infinite-scroll-component'
import sal from 'sal.js'

const IndexPage = ({ data }) => {
  const essay = data.allMicrocmsEssay.edges;
  const [listIndex, setListIndex] = useState(10)
  useEffect(() => {
    sal()
    if (typeof window.FONTPLUS !== "undefined"){
      window.FONTPLUS.reload()
    }
  });
  return (
    <Layout>
      <Seo title="" />
      <div className="p_index">
        <div className="p_index_inner inner_tb_pc">
          <InfiniteScroll
              dataLength={listIndex}
              next={() => setListIndex(listIndex + 10)}
              hasMore={essay.length >listIndex}
          >
           {essay.slice(0, listIndex).map (({ node }) => (
              <Essay data={node} key={node.id}/>
           ))}
        </InfiniteScroll>
        </div>
      </div>
    </Layout>
  )
}

export const query = graphql`{
  allMicrocmsEssay(sort: {publishedAt: DESC}) {
    edges {
      node {
        id
        publishedAt
        mainImage {
          height
          url
          width
        }
        photo {
          image {
            height
            url
            width
          }
          fieldId
        }
        text
        title
        essayId
      }
    }
  }
}`

export default IndexPage
