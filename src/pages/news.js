import React, { useState } from 'react';
import { graphql } from 'gatsby'
import Layout from "../components/layout"
import Seo from "../components/seo"
import NewsList from "../components/newslist"
import ArchiveList from "../components/archivelist"
import dayjs from "dayjs"

const NewsPage = ({data}) => {
  const news = data.allMicrocmsNews.edges;
  const newsYears = [];
  news.forEach(edge => {
    let newsYear = dayjs(edge.node.publishedAt).format(`YYYY`);
    if(newsYears.indexOf(newsYear) === -1){
      newsYears.push(newsYear);
    }
  })
  const [menuActive, setMenuActive] = useState(false);
  return (
    <Layout>
      <Seo title={`ニュース一覧`} />
      <div className="p_news">
        <div className="p_news_inner inner">
         <h1 className="p_news_ttl">NEWS</h1>
         <div className="p_works_tags">
            <p className="p_works_tags_ttl" aria-expanded={menuActive} onClick={() => setMenuActive(!menuActive)}>Archives</p>
            <div className="p_works_tags_list" aria-expanded={menuActive}>
              <ArchiveList data={newsYears} />
            </div>
          </div>
         <NewsList data={news} />
        </div>
      </div>
    </Layout>
  )
}

export const query = graphql`{
  allMicrocmsNews(sort: {publishedAt: DESC}) {
    edges {
      node {
        publishedAt
        photo {
          image {
            height
            url
            width
          }
        }
        text
        title
        newsId
      }
    }
  }
}`

export default NewsPage
