import React from 'react';
import { graphql } from "gatsby"
import Layout from "../components/layout"
import Seo from "../components/seo"
import WorksList from "../components/workslist"

const BooksPage = ({ data }) => {
  return (
    <Layout>
      <Seo title="Books一覧" />
      <div className="p_works">
        <div className="p_works_inner inner">
          <h1 className="p_works_ttl">BOOKS</h1>
          <WorksList data={data.allMicrocmsBooks.edges}  ctg="books"/>
        </div>
      </div>
    </Layout>
  )
}

export const query = graphql`{
  allMicrocmsBooks(sort: {publishedAt: DESC}) {
    edges {
      node {
        booksId
        publishedAt(formatString: "MMMM DD, YYYY")
        title
        text
        mainImage {
          height
          url
          width
        }
      }
    }
  }
}`

export default BooksPage
