import React,{useState,useEffect,useRef} from 'react';
import { Link} from "gatsby"
import Layout from "../components/layout"
import Seo from "../components/seo"
import queryString from 'query-string';

const ContactPage = ({ location }) => {

  const useInput = initialValue => {
    const [value, set] = useState(initialValue)
    return { value, onChange: (e) => {
      set(e.target.value)
    }
    }
  };
  const [subject, setSubject] = useState("お問い合わせ");
  const name = useInput("");
  const email = useInput("");
  const tel = useInput("");
  const company = useInput("");
  const text = useInput("");
  const select = useRef(null)


  const handleSubmit = async event => {
    event.preventDefault();
    handleCheck("finish")
    const params = {
      subject,
      name:name.value,
      email:email.value,
      tel:tel.value,
      company:company.value,
      text:text.value,
    }
    console.log(params)
    const response = await fetch("/api/contact", {
      method: "post",
      headers: {
        "Content-type": "application/x-www-form-urlencoded; charset=UTF-8",
      },
      body: new URLSearchParams(params).toString(),
    })
    console.log(response)
    const json = await response.json()
    if (response.status === 200) {
    } else {
      handleCheck("error")
    }
  };

  const [checkMode,setcheckMode] = useState("input")
  const handleCheck = (type) => {
    setcheckMode(type)
  };

  useEffect(() => {
    if (typeof window.FONTPLUS !== "undefined"){
      window.FONTPLUS.reload()
    }
    const {subject} = queryString.parse(location.search);
    if(subject === "book"){
      if (select.current) {
        select.current.value = "書籍に関するお問いあわせ"
        setSubject("書籍に関するお問いあわせ")
      }
    }
  },[])

  return (
    <Layout>
      <Seo title="コンタクト" />
      <div className="p_contact">
        <div className="p_contact_inner inner">
          {checkMode === "input" &&
          <>
          <div className="p_contact_head">
            <p className="p_contact_head_para">
              下記フォームよりお気軽にお問い合わせくださいませ。
              <br />
              後ほど、担当者よりご連絡させていただきます。
              <br />
              <span className="small">※は必須項目です。</span>
            </p>
          </div>
          <form className="p_contact_form" onSubmit={() => handleCheck("confirm")}>
            <div className="p_contact_block">
              <p className="p_contact_block_para">件名<span className="small">※</span></p>
              <div className="p_contact_block_input select">
                <select name="subject" onChange={(e) => {
                  setSubject(e.target.value)
                }} ref={select}>
                  <option>お問い合わせ</option>
                  <option>書籍に関するお問いあわせ</option>
                </select>
              </div>
            </div>
            <div className="p_contact_block">
              <p className="p_contact_block_para">お名前<span className="small">※</span></p>
              <div className="p_contact_block_input">
                <input name="name" required="required" type="text" {...name} />
              </div>
            </div>
            <div className="p_contact_block">
              <p className="p_contact_block_para">メールアドレス<span className="small">※</span></p>
              <div className="p_contact_block_input">
                <input type="email" name="email" required="required" {...email}/>
              </div>
            </div>
            <div className="p_contact_block">
              <p className="p_contact_block_para">電話番号※</p>
              <div className="p_contact_block_input">
                <input type="tel" name="tel" pattern="^[0-9-]+$" required="required"  {...tel}/>
              </div>
            </div>
            <div className="p_contact_block">
              <p className="p_contact_block_para">会社名</p>
              <div className="p_contact_block_input">
                <input name="company" type="text"  {...company} />
              </div>
            </div>
            <div className="p_contact_block">
              <p className="p_contact_block_para">お問い合わせ内容<span className="small">※</span></p>
              <div className="p_contact_block_input">
                <textarea
                  rows={10}
                  col={40}
                  name="text"
                  required="required"
                  {...text}
                 
                />
              </div>
            </div>
            <div className="p_contact_block_send">
              <input type="submit" value="確認" />
            </div>
          </form>
          </>
          }
          {checkMode === "confirm" &&
          <>
          <div className="p_contact_head">
            <p className="p_contact_head_para">
              下記の内容をご確認の上、送信ボタンを押してください。
            </p>
          </div>
          <div className="p_contact_form">
            <div className="p_contact_block">
              <p className="p_contact_block_para">件名<span className="small">※</span></p>
              <div className="p_contact_block_input">{subject}</div>
            </div>
            <div className="p_contact_block">
              <p className="p_contact_block_para">お名前<span className="small">※</span></p>
              <div className="p_contact_block_input">{name.value}</div>
            </div>
            <div className="p_contact_block">
              <p className="p_contact_block_para">メールアドレス<span className="small">※</span></p>
              <div className="p_contact_block_input">{email.value}</div>
            </div>
            <div className="p_contact_block">
              <p className="p_contact_block_para">電話番号※</p>
              <div className="p_contact_block_input">
                <div className="p_contact_block_input">{tel.value}</div>
              </div>
            </div>
            <div className="p_contact_block">
              <p className="p_contact_block_para">会社名</p>
              <div className="p_contact_block_input">{company.value}</div>

            </div>
            <div className="p_contact_block">
              <p className="p_contact_block_para">お問い合わせ内容<span className="small">※</span></p>
              <div className="p_contact_block_input">{text.value}</div>
            </div>
            <div className="p_contact_block_send">
              <input type="button" value="戻る" onClick={() => handleCheck("input")} />
              <input type="button" value="送信" onClick={handleSubmit} />
            </div>
          </div>
          </>
          }
          {checkMode === "finish" &&
          <div className="p_contact_head">
            <p className="p_contact_head_para">
            お問い合わせいただきありがとうございました。<br />
            お問い合わせを受け付けました。<br /><br />
            折り返し連絡いたしますので、恐れ入りますが、しばらくお待ちください。<br /><br />
            なお、ご入力いただいたメールアドレス宛に受付完了メールを配信しております。<br />
            完了メールが届かない場合、処理が正常に行われていない可能性があります。<br />
            大変お手数ですが、再度お問い合わせの手続きをお願い致します。
            </p>
            <p className="p_contact_head_link"><Link to={`/`}>トップへ戻る</Link></p>
          </div>
          }
          {checkMode === "error" &&
          <div className="p_contact_head">
            <p className="p_contact_head_para">
            エラーが発生しました。。<br />
            お手数ですが、再度フォームをご入力の上、送信ください。
            </p>
            <p className="p_contact_head_link"><Link to={`/contact`}>お問い合わせ</Link></p>
          </div>
          }
        </div>
      </div>
    </Layout>
  )
}

export default ContactPage
