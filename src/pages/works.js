//import React, { useState, useEffect, useRef } from 'react';
import React, { useState } from 'react';
import { graphql } from "gatsby"
import Layout from "../components/layout"
import Seo from "../components/seo"
import WorksList from "../components/workslist"
import TagList from "../components/taglist"

const WorksPage = ({ data }) => {
  const [menuActive, setMenuActive] = useState(false);
  return (
    <Layout>
      <Seo title="Works一覧" />
      <div className="p_works">
        <div className="p_works_inner inner">
          <h1 className="p_works_ttl">WORKS</h1>
          <div className="p_works_tags">
            <p className="p_works_tags_ttl" aria-expanded={menuActive} onClick={() => setMenuActive(!menuActive)}>Tags</p>
            <div className="p_works_tags_list" aria-expanded={menuActive}>
              <TagList data={data.allMicrocmsWorksTag.edges} />
            </div>
          </div>
          <WorksList data={data.allMicrocmsWorks.edges} ctg="works"/>
        </div>
      </div>
    </Layout>
  )
}

export const query = graphql`{
  allMicrocmsWorks(sort: {publishedAt: DESC}) {
    edges {
      node {
        worksId
        publishedAt(formatString: "MMMM DD, YYYY")
        title
        text
        tags {
          id
          title
        }
        mainImage {
          height
          url
          width
        }
        video
      }
    }
  }
  allMicrocmsWorksTag {
    edges {
      node {
        title
        worksTagId
      }
    }
  }
}`

export default WorksPage
