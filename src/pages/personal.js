import React from 'react';
import {graphql } from "gatsby"
import Layout from "../components/layout"
import Seo from "../components/seo"
import WorksList from "../components/workslist"

const WorksPage = ({ data }) => {

  return (
    <Layout>
      <Seo title="PERSONAL一覧" />
      <div className="p_works">
        <div className="p_works_inner inner">
          <h1 className="p_works_ttl">PERSONAL</h1>
          <WorksList data={data.allMicrocmsArtworks.edges}  ctg="personal"/>
        </div>
      </div>
    </Layout>
  )
}

export const query = graphql`{
  allMicrocmsArtworks(sort: {publishedAt: DESC}) {
    edges {
      node {
        artworksId
        publishedAt(formatString: "MMMM DD, YYYY")
        title
        photo {
          image {
            height
            url
            width
          }
        }
      }
    }
  }
}`

export default WorksPage
