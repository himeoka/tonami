import React from 'react';
import { graphql } from "gatsby"
import Layout from "../components/layout"
import Seo from "../components/seo"
import { StaticImage } from "gatsby-plugin-image"

const WorksPage = ({ data }) => {
  return (
    <Layout>
      <Seo title="プロフィール" />
      <div className="p_profile">
        <div className="p_profile_inner inner">
          <div className="p_profile_head">
            <h1 className="p_profile_head_ttl">砺波 周平<br />Shuhei Tonami</h1>
            <p className="p_profile_head_subttl">写真家</p>
            <div className="p_profile_head_image">
              <StaticImage src="../images/profile/profile.jpg" alt="砺波 周平" />
            </div>
          </div>
          <div className="p_profile_content">
            <div className="p_profile_block">
              <p className="">1979年北海道出身。<br />北里大学獣医畜産学部　生物生産環境学科卒業。<br />大学在学中から、写真家の細川剛氏に師事する。<br />キャンパスのあった青森県十和田市で、パートナーと一緒に廃材で古い家を直しながらくらすうちに、日々の何気ない時間の中にたくさんの感動があることを知る。<br />その暮らしの様子を3年間地元紙で連載。2007年東京都八王子市に東京事務所を置く傍ら、八ヶ岳南麓（長野県諏訪郡富士見町）に再び古い家を見つけ自分たちで改装し,妻と三人の娘、犬、猫と移り住む。<br />現在、東京都と長野、山梨に拠点を持ち活動中。</p>
            </div>
            <div className="p_profile_block">
              <p className="p_profile_block_ttl">・連載</p>
              <p className="p_profile_block_para">暮しの手帖 第五世紀より扉写真を毎号担当中。<br /><br />食の雑誌 dancyuの「食の絶滅危惧種」にて   日本全国の失われそうな食材を取材撮影中。<br />編集：里見美香　文章：瀬川慧<br />BEEK magazineにて「家族の風景」連載中。</p>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  )
}

export const query = graphql`{
  allMicrocmsWorks(sort: {publishedAt: DESC}) {
    edges {
      node {
        worksId
        publishedAt(formatString: "MMMM DD, YYYY")
        title
        text
        tags {
          id
          title
        }
        mainImage {
          height
          url
          width
        }
      }
    }
  }
  allMicrocmsWorksTag {
    edges {
      node {
        title
        worksTagId
      }
    }
  }
}`

export default WorksPage
