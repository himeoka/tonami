import React from "react"
import { Link} from "gatsby"

const TagList = ({ data,active }) => {
  return (
    <>
        <ul>
            {data.map (({ node }) => {
                let activeClass = node.worksTagId === active ? "active" : ""; 
                return (
                <li key={node.worksTagId} className={activeClass}>
                    <Link to={`/works/tags/${node.worksTagId}`}>{node.title}</Link>
                </li>
                )
            })}
        </ul>
    </>
  )
}

export default TagList