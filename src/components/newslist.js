import React, { useEffect,useState } from 'react';
import { Link } from "gatsby"
import moment from "moment"
import Imgix from "react-imgix"

const NewsBlock = ({news}) => {
  
  const dummyImage = {
    "url": "https://images.microcms-assets.io/assets/167b6357e0cb445b975f33e63e5216c4/0e63eef89e6247d28bb897fc6c833f69/newsDuumy.png",
    "height": 333,
    "width": 500
  }
  const image = news.photo.length ? news.photo[0].image : dummyImage;
  return (
    <>
      <figure className={"m_newslist_item_image " + (image.height > image.width ? "tate" : "")}>
        <Imgix
          className="lazyload"
          src={image.url}
          sizes="(max-width: 767px) 100vw, 330px"
          htmlAttributes={{
          alt: news.title,
          src:"data:image/gif;base64,R0lGODlhAQABAGAAACH5BAEKAP8ALAAAAAABAAEAAAgEAP8FBAA7"
          }}
          attributeConfig={{
            src: "data-src",
            srcSet: "data-srcset",
            sizes: "data-sizes",
          }}
          imgixParams={{q:85}}
        />
      </figure>
      <h2 className="m_newslist_item_ttl">{news.title}</h2>
      <p className="m_newslist_date">{moment(news.publishedAt).format(`YYYY.MM.DD`)}</p>
    </>
  )
}

const NewsList = ({ data }) => {
  const [today, setToday] = useState(false);
  useEffect(() => {
    let date = new Date();
    setToday(date)
    console.log(data)
  }, []);
  const dummyImage = {
    url:"https://images.microcms-assets.io/assets/167b6357e0cb445b975f33e63e5216c4/0e63eef89e6247d28bb897fc6c833f69/newsDuumy.png",
    height:333,
    width:500
  }
  return (
    <>
      <div className="m_newslist">
          {data.map (({ node }) => (
            <section data-sal="fade" data-sal-duration="1000" className="m_newslist_item" key={node.newsId}>
              <Link to={`/news/${node.newsId}`}>
                {(today - new Date(node.publishedAt))/ 86400000 < 90  && <p className="m_newslist_item_mark">NEW</p>}
                <NewsBlock news={node} />
              </Link>
            </section>
          ))}
      </div>
    </>
  )
}

export default NewsList