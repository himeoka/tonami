/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.com/docs/use-static-query/
 */
import React , { useEffect }from "react"
import PropTypes from "prop-types"

import Header from "./header"
import "../styles/theme.scss"
import { SiteProvider } from "../context/site-mode"
import { Helmet } from "react-helmet"
const Layout = ({children }) => {
  useEffect(() => {
  })
  return (
    <SiteProvider>
      <div className="wrapper">
        <Helmet>
        </Helmet>
        <Header />
        <main>{children}</main>
      </div>
    </SiteProvider>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
