import React from "react"
import moment from "moment"
import Imgix from "react-imgix"
import Share from "../components/share"

const Essay = props => {
  const data = props.data;
  const m1=60, m2=110, m3=170, m4=230, m5=290;
  const margins = [[m1,m4,m2,m3,m5,m2,m5,m3,m1,m4,m2,m1,m4,m3,m5],[m1, m2, m2, m4, m3, m1, m5, m2, m3, m5, m3, m5, m4, m4, m1],[m3, m1, m4, m1, m1, m2, m3, m5, m5, m5, m3, m4, m2, m2, m4],[m4, m5, m3, m5, m4, m2, m2, m5, m3, m1, m4, m3, m1, m2, m1],[m5, m2, m5, m4, m3, m2, m4, m1, m1, m2, m4, m5, m3, m3, m1],[m2, m4, m5, m2, m1, m5, m2, m3, m1, m3, m5, m4, m3, m4, m1],[m5, m1, m2, m5, m1, m4, m3, m3, m1, m3, m2, m4, m5, m2, m4],[m4, m2, m5, m5, m2, m5, m1, m1, m3, m2, m4, m4, m3, m1, m3],[m5, m5, m1, m2, m1, m4, m3, m4, m4, m5, m3, m2, m2, m1, m3],[m4, m2, m3, m4, m2, m1, m3, m1, m5, m3, m1, m2, m5, m5, m4],[m1, m4, m1, m1, m5, m2, m4, m2, m5, m4, m2, m5, m3, m3, m3],[m3, m3, m4, m1, m4, m4, m2, m5, m3, m2, m5, m1, m1, m2, m5],[m1, m5, m1, m2, m2, m4, m5, m2, m1, m4, m5, m4, m3, m3, m3],[m3, m3, m2, m3, m4, m4, m5, m1, m1, m5, m2, m5, m4, m2, m1],[m5, m5, m3, m2, m2, m1, m1, m4, m2, m4, m3, m4, m3, m1, m5],[m2, m5, m5, m1, m1, m3, m3, m4, m4, m3, m2, m4, m5, m2, m1],[m4, m3, m5, m2, m4, m3, m4, m1, m5, m2, m1, m1, m5, m2, m3],[m4, m5, m3, m2, m3, m3, m2, m1, m2, m4, m4, m1, m5, m5, m1],[m1, m2, m5, m2, m1, m3, m4, m3, m4, m2, m1, m3, m4, m5, m5],[m4, m1, m2, m2, m5, m5, m1, m2, m5, m3, m1, m4, m3, m4, m3],[m5, m2, m4, m5, m1, m3, m3, m1, m3, m1, m2, m4, m5, m2, m4],[m4, m5, m3, m3, m2, m3, m2, m4, m1, m2, m5, m1, m5, m4, m1],[m2, m3, m1, m3, m5, m4, m4, m2, m1, m2, m4, m3, m5, m1, m5],[m3, m2, m4, m4, m1, m2, m4, m5, m5, m1, m3, m2, m5, m3, m1],[m2, m1, m5, m2, m4, m5, m4, m1, m4, m3, m2, m3, m1, m5, m3],[m3, m2, m5, m4, m1, m5, m4, m3, m2, m1, m3, m2, m1, m4, m5],[m1, m5, m2, m4, m4, m2, m1, m2, m3, m1, m3, m5, m5, m3, m4],[m3, m2, m5, m4, m4, m3, m1, m3, m5, m2, m4, m2, m5, m1, m1],[m5, m1, m4, m2, m3, m1, m2, m4, m3, m1, m5, m4, m5, m2, m3],[m5, m3, m3, m5, m5, m2, m1, m3, m1, m4, m2, m4, m1, m4, m2],[m2, m1, m2, m5, m3, m1, m5, m3, m2, m4, m4, m3, m5, m1, m4],[m2, m5, m4, m5, m5, m1, m2, m1, m3, m4, m1, m4, m2, m3, m3],[m4, m2, m5, m2, m3, m2, m4, m5, m1, m3, m1, m4, m5, m1, m3],[m1, m2, m4, m3, m2, m3, m5, m1, m1, m4, m5, m4, m2, m3, m5],[m5, m1, m4, m2, m5, m3, m2, m1, m3, m2, m4, m3, m1, m4, m5],[m4, m3, m4, m1, m2, m1, m3, m5, m4, m5, m2, m3, m2, m5, m1],[m5, m5, m3, m1, m2, m4, m3, m1, m4, m5, m3, m4, m1, m2, m2],[m5, m5, m3, m1, m5, m4, m1, m4, m3, m2, m1, m2, m2, m4, m3],[m3, m4, m3, m5, m5, m4, m4, m2, m1, m1, m2, m3, m1, m2, m5],[m1, m5, m1, m2, m5, m4, m2, m3, m3, m1, m3, m5, m2, m4, m4],[m4, m5, m2, m5, m2, m5, m2, m1, m3, m4, m4, m1, m1, m3, m3],[m1, m2, m5, m2, m3, m3, m1, m5, m2, m5, m1, m4, m4, m4, m3],[m2, m5, m4, m1, m4, m1, m3, m3, m5, m1, m3, m2, m2, m4, m5],[m4, m5, m2, m3, m2, m1, m3, m5, m1, m2, m4, m4, m1, m3, m5],[m4, m3, m5, m2, m1, m2, m4, m3, m2, m5, m1, m4, m1, m5, m3],[m4, m3, m4, m2, m2, m1, m5, m3, m2, m1, m3, m4, m5, m5, m1],[m4, m3, m4, m3, m5, m3, m1, m2, m2, m5, m1, m5, m2, m1, m4],[m2, m2, m4, m3, m1, m5, m3, m1, m5, m4, m5, m3, m2, m4, m1],[m5, m3, m4, m1, m4, m3, m2, m2, m1, m4, m5, m3, m1, m2, m5],[m1, m1, m2, m3, m4, m3, m5, m2, m3, m5, m1, m4, m2, m4, m5],[m3, m2, m4, m5, m4, m1, m5, m1, m5, m4, m3, m2, m1, m2, m3],[m3, m5, m4, m5, m2, m3, m4, m1, m1, m4, m5, m2, m1, m3, m2],[m2, m1, m2, m3, m4, m4, m3, m2, m3, m1, m5, m1, m5, m5, m4]];  const id = data.essayId;
  const marginLength = 15;
  const key = id.slice( -1 ) ;
  function convertToInt(a) {
    const alphabet = "abcdefghijklmnopqrstuvwxyz";
    return alphabet.indexOf(a) + 1 ;
  }
  const margin =  margins[convertToInt(key)]
  return (
    <>
      <section className='m_essay'>
        <div data-sal="fade" data-sal-duration="1200" className={'m_essay_image m_essay_mainimage image ' + (data.mainImage.height/data.mainImage.width > 1 ? 'tate' : '')} >
          <figure style={{paddingTop: (data.mainImage.height/data.mainImage.width)*100 + "%"}}>
            <Imgix
              className="lazyload"
              src={data.mainImage.url}
              sizes="(max-width: 767px) 100vw, 700px"
              htmlAttributes={{
              alt: data.title,
              src:"data:image/gif;base64,R0lGODlhAQABAGAAACH5BAEKAP8ALAAAAAABAAEAAAgEAP8FBAA7"
              }}
              attributeConfig={{
                src: "data-src",
                srcSet: "data-srcset",
                sizes: "data-sizes",
              }}
              imgixParams={{q:85}}
            />
          </figure>
        </div>
        <div className="m_essay_pics">
           {data.photo.map (({ image},index) => {
             let activeIndex = index < marginLength ? index : index - marginLength;
             return(
              <div data-sal="fade" data-sal-duration="1200" className={'m_essay_image image ' + (image.height/image.width > 1 ? 'tate' : '')} key={index} style={{marginBottom: margin[activeIndex] + "px"}} >
                <figure style={{paddingTop: (image.height/image.width)*100 + "%"}}>
                  <Imgix
                    className="lazyload"
                    src={image.url}
                    sizes="(max-width: 700px) 100vw, 700px"
                    htmlAttributes={{
                    alt: data.title,
                    src:"data:image/gif;base64,R0lGODlhAQABAGAAACH5BAEKAP8ALAAAAAABAAEAAAgEAP8FBAA7"
                    }}
                    attributeConfig={{
                    src: "data-src",
                    srcSet: "data-srcset",
                    sizes: "data-sizes",
                    }}
                    imgixParams={{q:85}}
                  />
                </figure>
              </div>
           )})}
        </div>
        <div className="m_essay_head inner_sp" data-sal="slide-up" data-sal-duration="1200">
          <Share title={data.title} id={data.essayId}/>
          <p className="m_essay_para">{moment(data.publishedAt).format(`YYYY.MM.DD`)}</p>
          <h2 className="m_essay_ttl">{data.title}</h2>
          <div className="m_essay_text" dangerouslySetInnerHTML={{
            __html: data.text
          }}>
          </div>
        </div>
      </section>
    </>
  )
}

export default Essay