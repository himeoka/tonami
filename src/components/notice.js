import React from "react"
import moment from "moment"

const Notice = ({data})=> {
  return (
    <>
      <div className="m_notice">
        <div className="m_notice_inner inner">
          {data.map((news,index) => (
            <div className="m_notice_block" key={index}>
              <p className="m_notice_block_ttl">{news.title}</p>
              <div className="m_notice_block_para" dangerouslySetInnerHTML={{__html: news.text}}></div>
              <p className="m_notice_block_date">（{moment(news.date).format(`YYYY.MM.DD`)}）</p>
            </div>
          )
          )}
        </div>
      </div>
    </>
  )
}

export default Notice