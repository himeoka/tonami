import React, { useState,useRef}from 'react'
import {StaticQuery,graphql} from "gatsby"
import ShareBtn from "../images/cmn/icon_sharebtn.inline.svg";
import LinkBtn from "../images/cmn/icon_link.inline.svg";
import FBtn from "../images/cmn/icon_fb.inline.svg";
import TWBtn from "../images/cmn/icon_tw.inline.svg";


const Share = ({title,id})=> {
  const [shareActive, setShareActive] = useState(false);
  const [copyActive, setCopyActive] = useState(false);

  const copy = url => {
    navigator.clipboard.writeText(url).then(e => {
      setCopyActive("on")
    });
  };
  return (
    <StaticQuery
    query={graphql`
    query ShareQuery {
      site {
        siteMetadata {
          title
          siteUrl
        }
      }
    }
  `}
    render={data => {
      return(
      <>
        <div className="m_share">
          <p className="m_share_btn"onClick={() => setShareActive(!shareActive)}><ShareBtn /></p>
          <ul className="m_share_list" aria-expanded={shareActive}>
            <li><span className={"link copy " + copyActive} href="" onClick={() => copy(`${data.site.siteMetadata.siteUrl}/essay/${id}`)}><LinkBtn /></span></li>
            <li><a target="_blank" href={`https://www.facebook.com/sharer/sharer.php?u=${data.site.siteMetadata.siteUrl}/essay/${id}`}><FBtn /></a></li>
            <li><a target="_blank" href={`https://twitter.com/share?url=${data.site.siteMetadata.siteUrl}/essay/${id}&text=${encodeURIComponent(title + " | " + data.site.siteMetadata.title)}`}><TWBtn /></a></li>
          </ul>
        </div>
      </>
    )}}
    />
  )
}

export default Share