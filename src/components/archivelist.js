import React from "react"
import { Link} from "gatsby"
import moment from "moment"

const ArchiveList = ({ data,active }) => {
  let year = "";
  return (
    <>
        <ul>
            {data.map((item,index) => {
                if(year !== moment(item).format(`YYYY`)){
                  year = moment(item).format(`YYYY`);
                  let activeClass = year === active ? "active" : ""; 
                  return (
                  <li key={index} className={activeClass}>
                    <Link to={`/news/${item}`}>{item}</Link>
                  </li>
                  )
                }else{
                  return false
                }
            })}
        </ul>
    </>
  )
}

export default ArchiveList