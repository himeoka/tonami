// Import Swiper React components
import React , {useEffect,useState,useRef}from 'react';
import { Swiper, SwiperSlide } from "swiper/react";
import Imgix from "react-imgix"
// Import Swiper styles
import 'swiper/css';
import '../styles/components/swiper.scss';


import SwiperCore, { Keyboard, Mousewheel } from "swiper/core";
SwiperCore.use([Keyboard, Mousewheel]);

export default ({data,title,movie}) => {
  const [iframeActive, setIframeActive] = useState("");
  const [iframeRatio, setIframeRatio] = useState(0);
  const videoElement = useRef(null);
  let timer = false;
  const handleClickIframe = () => {
    if (timer !== false) {
      clearTimeout(timer);
    }
    setIframeActive("on");
    timer = setTimeout(function() {
      setIframeActive("");
  }, 5000);
  }
  const [activeIndex, setActiveIndex] = useState(0);
  const getActiveIndex = (swiper) => {
    setActiveIndex(swiper.activeIndex+1);
  }
  useEffect(() => {
    if(movie){
      setIframeRatio(videoElement.current.children[0].height / videoElement.current.children[0].width * 100)
    }
  })
  let length = data.length;
  if(movie){
    length += 1;
  }
  return (
    <div>
    <Swiper
      spaceBetween={50}
      slidesPerView={1}
      mousewheel={true}
      direction={'vertical'}
      speed = {1000}
      className="m_swiper"
      onSlideChangeTransitionStart={getActiveIndex}
      onInit={getActiveIndex}
      keyboard={{
        enabled: true,
        pageUpDown:true
      }}

    >
      {movie &&  <SwiperSlide className="m_swiper_slide"><div onClick={handleClickIframe} dangerouslySetInnerHTML={{__html: movie}} ref={videoElement} className={"m_swiper_slide_video " + iframeActive} style={{paddingTop:iframeRatio + '%'}}></div></SwiperSlide>}
      {data.map ((node,index) => (
        <SwiperSlide className="m_swiper_slide" key={index}>
          <Imgix
            className="lazyload"
            src={node.image.url}
            sizes="(max-width: 767px) 100vw, 1100px"
            htmlAttributes={{
            alt: title,
            src:"data:image/gif;base64,R0lGODlhAQABAGAAACH5BAEKAP8ALAAAAAABAAEAAAgEAP8FBAA7"
            }}
            attributeConfig={{
              src: "data-src",
              srcSet: "data-srcset",
              sizes: "data-sizes",
            }}
            imgixParams={{q:85}}
          />
        </SwiperSlide>
      ))}
    </Swiper>
    <p className="m_swiper_num">{activeIndex}/{length}</p>
    </div>
  );
};