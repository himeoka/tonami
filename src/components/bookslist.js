import React from "react"
import { Link } from "gatsby"
import Imgix from "react-imgix"
import moment from "moment"

const BooksList = ({ data }) => {
  return (
    <>
      <div className="m_workslist">
          {data.map (({ node },index) => (
            <section data-sal="fade" data-sal-duration="1000" className="m_workslist_item" key={index}>
              <Link to={`/books/${node.booksId}`}>
                <figure  className={"m_workslist_item_image " + (node.mainImage.height > node.mainImage.width ? "tate" : "")}>
                  <Imgix
                    className="lazyload"
                    src={node.mainImage.url}
                    sizes="(max-width: 767px) 100vw, 400px"
                    htmlAttributes={{
                    alt: node.title,
                    src:"data:image/gif;base64,R0lGODlhAQABAGAAACH5BAEKAP8ALAAAAAABAAEAAAgEAP8FBAA7"
                    }}
                    attributeConfig={{
                      src: "data-src",
                      srcSet: "data-srcset",
                      sizes: "data-sizes",
                    }}
                    imgixParams={{q:85}}
                  />
                </figure>
                <h2 className="m_workslist_item_ttl">{node.title}</h2>
                <p className="m_workslist_item_date">{moment(node.publishedAt).format(`YYYY.MM.DD`)}</p>
              </Link>
            </section>
          ))}
      </div>
    </>
  )
}

export default BooksList