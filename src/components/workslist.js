import React, { createContext, useRef,useEffect,useContext} from 'react'
import { ObserverContext } from "../provider/IntersectionObserverProvider";
import { handleObserver } from '../utils/IntersectionObserver'
import { Link } from "gatsby"
import Imgix from "react-imgix"
import dayjs from 'dayjs';

const WorksList = ({ data,ctg }) => {
  const { toTargets, targets } = useContext(ObserverContext);

  useEffect(() => {
    handleObserver(targets)
  }, [])
  let idString = ctg === "personal" ? "artworksId" : ctg + "Id";
  return (
    <>
      <div className="m_workslist">
          {data.map (({ node }) => {
            let thumbnail = "";
            if(node.mainImage){
              thumbnail = node.mainImage;
            }else{
              thumbnail = node.photo[0].image;
            }
            return(
              <section data-sal="fade" data-sal-duration="1000" className="m_workslist_item" key={node[idString]}>
                <Link to={`/${ctg}/${node[idString]}`}>
                  {node.video &&
                    <div ref={toTargets} className="m_workslist_item_video"dangerouslySetInnerHTML={{
                      __html:node.video
                        }}></div>
                  }
                  {!node.video &&
                    <figure className={"m_workslist_item_image " + (thumbnail.height > thumbnail.width ? "tate" : "")}>
                      <Imgix
                        className="lazyload"
                        src={thumbnail.url}
                        sizes="(max-width: 767px) 100vw, 400px"
                        htmlAttributes={{
                        alt: node.title,
                        src:"data:image/gif;base64,R0lGODlhAQABAGAAACH5BAEKAP8ALAAAAAABAAEAAAgEAP8FBAA7"
                        }}
                        attributeConfig={{
                          src: "data-src",
                          srcSet: "data-srcset",
                          sizes: "data-sizes",
                        }}
                        imgixParams={{q:85}}
                      />
                    </figure>
                  }
                  <h2 className="m_workslist_item_ttl">{node.title}</h2>
                  {ctg === "books" &&
                    <p className="m_workslist_item_date">{dayjs(node.publishedAt).format(`YYYY.MM.DD`)}</p>
                  }
                </Link>
              </section>
          )})}
      </div>
    </>
  )
}

export default WorksList