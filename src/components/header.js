import React, { useState, useEffect }from 'react'
import { Link , useStaticQuery, graphql } from "gatsby"
import logoInsta from "../images/cmn/icon_insta.svg";
import { useSiteContext} from "../context/site-mode"
import moment from "moment"

const Header = () => {
    const { page, setPage } = useSiteContext()
    const [menuActive, setMenuActive] = useState(false);
    const [scrollClass, setscrollClass] = useState("");
    const [today, setToday] = useState(false);
    const closeWithClickOutSideMethod = (e, setter) => {
      if (e.target === e.currentTarget) {
        setter(false);
      } else {
      }
    };
    useEffect(() => {
      let scroll = 0;
      let checker = "";
      let handleScroll = function(e){
        if(window.scrollY　> scroll && checker){
          setscrollClass("");
          checker = false;
        }else if(window.scrollY　< scroll && !checker){
          setscrollClass("fix")
          checker = true;
        }
        if(window.scrollY < 300 && checker){
          setscrollClass("");
          checker = false;
        }
        scroll = window.scrollY;
      }
      window.addEventListener("scroll",handleScroll);
      return () => {
        window.removeEventListener("scroll", handleScroll);
      };
    });
    useEffect(() => {
      let date = new Date();
      setToday(date)
    }, []);

    const data = useStaticQuery(
      graphql`{
  allMicrocmsEssay {
    edges {
      node {
        id
        publishedAt(formatString: "YYYY")
      }
    }
  }
  allMicrocmsNews(sort: {publishedAt: DESC}, limit: 1) {
    edges {
      node {
        newsId
        publishedAt
      }
    }
  }
}`
    );
    const years = [];
    data.allMicrocmsEssay.edges.forEach(elem => {
      if(years.indexOf(elem.node.publishedAt) === -1){
        years.push(elem.node.publishedAt)
      }
    });
    years.sort(function(a, b) {
      return b - a;
    });
    const newsClass = new Date() - new Date(data.allMicrocmsNews.edges[0].node.publishedAt)/ 86400000 < 90 ? " on" : "";
    return (
      <div className="m_head_wrapper">
        <header className={"m_head " +  scrollClass} aria-expanded={menuActive}>
          <div className="m_head_top" aria-expanded={menuActive}>
            <div className="m_head_inner">
              <div className="m_head_logo">
                {page
                    ? <p className="m_head_logo_arrow"><Link to={`/${page}/`}></Link></p>
                    : <p className="m_head_logo_ttl"><Link to={`/`}>砺波周平</Link></p>
                }
              </div>
              <div className="m_head_right">
                <p className={"m_head_news" + newsClass} aria-expanded={menuActive}><Link to={`/news/${data.allMicrocmsNews.edges[0].node.newsId}`}>NEWS {moment(data.allMicrocmsNews.edges[0].node.publishedAt).format(`M.DD`)}</Link></p>
                <div className="m_head_btn" aria-controls="m_head_menu" aria-expanded={menuActive} onClick={() => setMenuActive(!menuActive)}>
                  <div className="m_head_btn_inner">
                    <span></span>
                    <span></span>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="m_head_menu_outer" aria-expanded={menuActive} onClick={(e) => {closeWithClickOutSideMethod(e, setMenuActive);}}>
            <div className="m_head_menu">
              <div className="m_head_btn" aria-controls="m_head_menu" aria-expanded={menuActive} onClick={() => setMenuActive(!menuActive)}>
                <div className="m_head_btn_inner">
                  <span></span>
                  <span></span>
                </div>
              </div>
              <div className="m_head_menu_box">
                <p className="m_head_menu_list_li"><Link to={`/`}>HOME</Link></p>
              </div>
              <div className="m_head_menu_box">
                <div className="m_head_menu_left">
                  <ul className="m_head_menu_list">
                    <li className="m_head_menu_list_li"><Link to={`/essay/${years[0]}`}>ESSAY</Link>
                      <ul className="m_head_menu_sublist">
                        {years.map(year => (
                          <React.Fragment key={year}>
                            <li className="m_head_menu_sublist_li"><Link to={`/essay/${year}`}>・{year}</Link></li>
                          </React.Fragment>
                        ))}
                      </ul>
                    </li>
                  </ul>
                </div>
                <div className="m_head_menu_right">
                  <ul className="m_head_menu_list">
                  <li className="m_head_menu_list_li"><Link to={`/news`}>NEWS</Link></li>
                  <li className="m_head_menu_list_li"><Link to={`/personal`}>PERSONAL</Link></li>
                  <li className="m_head_menu_list_li"><Link to={`/works`}>WORKS</Link></li>
                  <li className="m_head_menu_list_li"><Link to={`/books/`}>BOOKS</Link></li>
                  <li className="m_head_menu_list_li"><Link to={`/profile`}>PROFILE</Link></li>
                  <li className="m_head_menu_list_li"><Link to={`/contact/`}>CONTACT</Link></li>
                  <li className="m_head_menu_list_li"><a href="https://www.instagram.com/tonamishuhei/" target="_blank" rel="noreferrer"><img src={logoInsta} alt="INSTAGRAM" className="insta" /></a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </header>
      </div>
    )
}

export default Header
