import React from 'react'
import 'lazysizes'
import { IntersectionObserverProvider } from "./src/provider/IntersectionObserverProvider"

export const onInitialClientRender = () => {
  function loadScript(src, callback) {
    let script = document.createElement('script');
    script.src = src;
    script.onload = () => callback(script); // callback関数 に script を渡す。
    document.head.append(script);
  }
  loadScript('https://webfont.fontplus.jp/accessor/script/fontplus.js?HtR~RpHzLnc%3D&box=PWzSwVOn7Jo%3D&aa=1&ab=2', script => {
    window.FONTPLUS.reload()
  });
  window.lazySizes.cfg.preloadAfterLoad = true
}

export const onRouteUpdate = ({ location, prevLocation }) => {
  if (typeof window.FONTPLUS !== "undefined"){
    window.FONTPLUS.reload()
  }
}


export const wrapRootElement = ({ element }) => (
  <IntersectionObserverProvider>{element}</IntersectionObserverProvider>
)
